package org.newyork.times.news

import androidx.core.view.isVisible
import androidx.fragment.app.testing.launchFragmentInContainer
import androidx.test.ext.junit.runners.AndroidJUnit4
import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import junit.framework.Assert.assertEquals
import kotlinx.coroutines.flow.flow
import org.junit.Assert.assertTrue
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.newyork.times.R
import org.newyork.times.annotation.ApplicationScope
import org.newyork.times.mvi.ViewState.Data
import org.newyork.times.mvi.ViewState.Empty
import org.newyork.times.mvi.ViewState.Error
import toothpick.testing.ToothPickRule

private const val BACKGROUND = "BACKGROUND"
private const val HEADLINE = "HEADLINE"
private const val URL = "URL"
private const val ABSTRACT = "ABSTRACT"

@RunWith(AndroidJUnit4::class)
class ArticleListFragmentTest {

    @get:Rule
    var toothPickRule: ToothPickRule = ToothPickRule(this)

    private val article by lazy {
        Article(background = BACKGROUND, headline = HEADLINE, url = URL, abstract = ABSTRACT)
    }

    private val exhibitsViewModel = mockk<ArticleViewModel>()

    private val scenario by lazy {
        launchFragmentInContainer<ArticleListFragment>(themeResId = R.style.Theme_MaterialComponents)
    }

    @Before
    fun setUp() {
        toothPickRule.testModule.bind(ArticleViewModel::class.java).toInstance(exhibitsViewModel)
        toothPickRule.setScopeName(ApplicationScope::class.java)

        every { exhibitsViewModel.flow } returns flow { emit(Empty<ArticleViewState>()) }
        every { exhibitsViewModel.load() } returns Unit
    }

    @Test
    fun `Test Error state`() {
        every { exhibitsViewModel.flow } returns flow { emit(Error<ArticleViewState>(Throwable("error"))) }

        scenario.onFragment { assertTrue(it.binding.view.errorMessage.isVisible) }
    }

    @Test
    fun `Test empty state`() {
        every { exhibitsViewModel.flow } returns flow { emit(Data(ArticleViewState(listOf()))) }

        scenario.onFragment { assertTrue(it.binding.view.errorMessage.isVisible) }
    }

    @Test
    fun `Test Non Empty List state`() {
        every { exhibitsViewModel.flow } returns flow { emit(Data(ArticleViewState(listOf(article)))) }

        scenario.onFragment { fragment ->
            assertEquals(1, fragment.binding.view.articles.adapter?.itemCount)
            assertTrue(fragment.binding.view.articles.isVisible)
        }
    }

    @Test
    fun `Test load called on empty`() {
        every { exhibitsViewModel.load() } returns Unit

        scenario.onFragment { verify(exactly = 1) { exhibitsViewModel.load() } }
    }
}
