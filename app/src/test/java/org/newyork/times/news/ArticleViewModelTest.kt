package org.newyork.times.news

import androidx.test.ext.junit.runners.AndroidJUnit4
import io.mockk.every
import io.mockk.mockk
import kotlinx.coroutines.flow.flow
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.newyork.times.mvi.ViewState
import org.newyork.times.rules.CoroutinesTestRule
import org.newyork.times.test

@RunWith(AndroidJUnit4::class)
class ArticleViewModelTest {
    @get:Rule
    val coroutinesTestRule = CoroutinesTestRule()
    private val exhibitRepository = mockk<ArticleRepository>()
    private val viewModel: ArticleViewModel =
        ArticleViewModel(exhibitRepository, coroutinesTestRule.dispatcherProvider)

    @Test
    fun `Test Load Intent`() = coroutinesTestRule.test {
        val collector = viewModel.flow.test(this)

        every { exhibitRepository.exhibits } returns flow { emit(listOf<Article>()) }

        viewModel.load()

        collector
            .assertValueAt(0) { it is ViewState.Empty }
            .assertValueAt(1) { it is ViewState.Data }
            .dispose()
    }

    @Test
    fun `Test Empty Initial State`() = coroutinesTestRule.test {
        viewModel.flow.test(this).assertValueAt(0) { it is ViewState.Empty }.dispose()
    }
}
