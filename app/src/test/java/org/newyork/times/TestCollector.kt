package org.newyork.times

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Job
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import org.junit.Assert

data class TestCollector<T>(val job: Job, val results: MutableList<T>) {
    fun dispose() = job.cancel()
    fun clearResults() = results.clear()

    fun assertIsActive() = apply { Assert.assertTrue(job.isActive) }
    fun assertIsComplete() = apply { Assert.assertTrue(job.isCompleted) }
    fun assertValueCount(expectedCount: Int) = apply {
        Assert.assertEquals(
            expectedCount,
            results.size
        )
    }

    fun assertValueAt(index: Int, predicate: (T) -> Boolean) = apply {
        Assert.assertTrue(
            predicate(
                results[index]
            )
        )
    }

    fun assertValue(predicate: (T) -> Boolean) = assertValueCount(1).assertValueAt(0, predicate)
    fun assertValuesOnly(vararg values: T) = assertIsActive().apply {
        values.forEachIndexed { index, t -> Assert.assertEquals(results[index], t) }
    }
}

fun <T> Flow<T>.test(coroutineScope: CoroutineScope): TestCollector<T> {
    val results = mutableListOf<T>()
    return TestCollector(coroutineScope.launch { collect(results::add) }, results)
}
