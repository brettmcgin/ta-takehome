package org.newyork.times

import android.app.Application
import com.github.stephanenicolas.toothpick.smoothie.BuildConfig
import org.newyork.times.annotation.ApplicationScope
import org.newyork.times.injection.DispatchersProvider
import org.newyork.times.mvi.DispatcherProvider
import toothpick.Scope
import toothpick.Toothpick
import toothpick.configuration.Configuration
import toothpick.ktp.KTP
import toothpick.ktp.binding.bind
import toothpick.ktp.binding.module
import toothpick.smoothie.module.SmoothieApplicationModule

class NeyYorkTimesApplication : Application() {
    @Suppress("LateinitUsage")
    lateinit var scope: Scope

    override fun onCreate() {
        super.onCreate()

        Toothpick.setConfiguration(
            if (BuildConfig.DEBUG) Configuration.forDevelopment() else Configuration.forProduction()
        )

        scope = KTP.openScope(ApplicationScope::class.java).installModules(
            SmoothieApplicationModule(this),
            module {
                bind<DispatcherProvider>().toProvider(DispatchersProvider::class)
            }
        )
    }

    override fun onTrimMemory(level: Int) {
        super.onTrimMemory(level)
        scope.release()
    }
}
