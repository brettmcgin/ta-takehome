package org.newyork.times.injection

import org.newyork.times.mvi.DispatcherProvider
import toothpick.InjectConstructor
import javax.inject.Provider

@InjectConstructor
class DispatchersProvider : Provider<DispatcherProvider> {
    override fun get(): DispatcherProvider = object : DispatcherProvider {}
}
