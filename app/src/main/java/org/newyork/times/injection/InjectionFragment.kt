package org.newyork.times.injection

import android.os.Bundle
import androidx.fragment.app.Fragment
import org.newyork.times.annotation.ApplicationScope
import toothpick.ktp.KTP
import toothpick.smoothie.lifecycle.closeOnDestroy

open class InjectionFragment : Fragment() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        KTP.openScopes(ApplicationScope::class.java, requireActivity(), this)
            .closeOnDestroy(this)
            .inject(this)
    }
}
