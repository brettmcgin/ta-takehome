package org.newyork.times.flow

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch

inline fun <T> Flow<T>.collectIn(
    scope: CoroutineScope,
    crossinline block: suspend (T) -> Unit
) = scope.launch { collect(block) }
