package org.newyork.times.extentions

val Any?.isNull: Boolean get() = this == null

val Any?.isNotNull: Boolean get() = !this.isNull
