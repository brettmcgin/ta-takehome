package org.newyork.times.service

import com.jakewharton.retrofit2.converter.kotlinx.serialization.asConverterFactory
import kotlinx.serialization.ExperimentalSerializationApi
import kotlinx.serialization.json.Json
import okhttp3.MediaType.Companion.toMediaType
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.newyork.times.news.Article
import retrofit2.Retrofit
import toothpick.InjectConstructor
import java.net.URL

private const val EXHIBITS_URL = "https://api.nytimes.com"

@InjectConstructor
class NewYorkTimesClient {
    private val baseUrl = URL(EXHIBITS_URL)

    private val contentType = "application/json".toMediaType()
    private val httpLoggingInterceptor =
        HttpLoggingInterceptor().apply { level = HttpLoggingInterceptor.Level.BODY }
    private val okHttpClient = OkHttpClient.Builder().addInterceptor(httpLoggingInterceptor).build()
    private val json = Json {
        encodeDefaults = true
        isLenient = true
        ignoreUnknownKeys = true
    }

    @ExperimentalSerializationApi
    private val client: ArticleService by lazy {
        Retrofit.Builder()
            .baseUrl(baseUrl)
            .addConverterFactory(json.asConverterFactory(contentType))
            .client(okHttpClient)
            .build()
            .create(ArticleService::class.java)
    }

    @ExperimentalSerializationApi
    suspend fun fetchRecent(): List<Article> = client.fetchRecent().results
        .filter { it.title.isNotEmpty() }
        .map { apiResult ->
            with(apiResult) {
                Article(
                    headline = title,
                    background = multimedia.firstOrNull()?.url.orEmpty(),
                    url = url,
                    abstract = abstract
                )
            }
        }
}
