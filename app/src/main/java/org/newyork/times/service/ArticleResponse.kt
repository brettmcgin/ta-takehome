package org.newyork.times.service

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class ArticleResponse(
    @SerialName("copyright") val copyright: String = "",
    @SerialName("last_updated") val lastUpdated: String = "",
    @SerialName("num_results") val numResults: Int = 0,
    @SerialName("results") val results: List<Result> = emptyList(),
    @SerialName("section") val section: String = "",
    @SerialName("status") val status: String = "",
)

@Serializable
data class Result(
    @SerialName("abstract") val abstract: String = "",
    @SerialName("byline") val byline: String = "",
    @SerialName("created_date") val createdDate: String = "",
    @SerialName("des_facet") val desFacet: List<String> = emptyList(),
    @SerialName("geo_facet") val geoFacet: List<String> = emptyList(),
    @SerialName("item_type") val itemType: String = "",
    @SerialName("kicker") val kicker: String = "",
    @SerialName("material_type_facet") val materialTypeFacet: String = "",
    @SerialName("multimedia") val multimedia: List<Multimedia> = emptyList(),
    @SerialName("org_facet") val orgFacet: List<String> = emptyList(),
    @SerialName("per_facet") val perFacet: List<String> = emptyList(),
    @SerialName("published_date") val publishedDate: String = "",
    @SerialName("section") val section: String = "",
    @SerialName("short_url") val shortUrl: String = "",
    @SerialName("subsection") val subsection: String = "",
    @SerialName("title") val title: String = "",
    @SerialName("updated_date") val updatedDate: String = "",
    @SerialName("uri") val uri: String = "",
    @SerialName("url") val url: String = "",
)

@Serializable
data class Multimedia(
    @SerialName("caption") val caption: String = "",
    @SerialName("copyright") val copyright: String = "",
    @SerialName("format") val format: String = "",
    @SerialName("height") val height: Int = 0,
    @SerialName("subtype") val subtype: String = "",
    @SerialName("type") val type: String = "",
    @SerialName("url") val url: String = "",
    @SerialName("width") val width: Int = 0,
)
