package org.newyork.times.service

import org.newyork.times.BuildConfig.API_KEY
import retrofit2.http.GET
import retrofit2.http.Query

private const val QUERY_PATH = "svc/topstories/v2/arts.json"

interface ArticleService {
    @GET(QUERY_PATH)
    suspend fun fetchRecent(@Query("api-key", encoded = true) apiKey: String = API_KEY): ArticleResponse
}
