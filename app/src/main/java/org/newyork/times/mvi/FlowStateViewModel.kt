package org.newyork.times.mvi

import android.util.Log
import androidx.annotation.CallSuper
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.SharingStarted
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.flowOn
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.onCompletion
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.flow.onStart
import kotlinx.coroutines.flow.shareIn
import kotlinx.coroutines.launch

private const val TAG = "FlowStateViewModel"

abstract class FlowStateViewModel<Action, Data>(
    ioDispatcher: CoroutineDispatcher = Dispatchers.IO
) : ViewModel() {
    private val actionFlow = MutableSharedFlow<Action>()

    private var state: ViewState<Data>? = null
        @Synchronized get

    @Suppress("UnnecessaryLet")
    val flow: Flow<ViewState<Data>> = actionFlow
        .let(::transformFlow)
        .catch { Log.d(TAG, "transformFlow emitted an error: $it") }
        .onCompletion { Log.d(TAG, "transformFlow completed unexpectedly!") }
        .flowOn(ioDispatcher)
        .onEach { state = it }
        .shareIn(viewModelScope, SharingStarted.Eagerly)
        .onStart { emit(state ?: ViewState.Empty()) }

    @CallSuper
    open fun clearState() { state = null }

    protected fun doAction(action: Action) {
        viewModelScope.launch(Dispatchers.Main) { actionFlow.emit(action) }
    }

    protected fun Flow<Data>.toViewState(preserveDataOnError: Boolean = false): Flow<ViewState<Data>> =
        map<Data, ViewState<Data>> { data -> ViewState.Data(data) }.catch { error ->
            emit(ViewState.Error(error, if (preserveDataOnError) state?.data else null))
        }

    protected abstract fun transformFlow(intentStream: Flow<Action>): Flow<ViewState<Data>>
}
