package org.newyork.times.mvi

sealed class ViewState<out D>(open val data: D? = null, open val error: Throwable? = null) {
    class Empty<D> : ViewState<D>()
    class Error<D>(override val error: Throwable, previousData: D? = null) : ViewState<D>(previousData)
    class Data<D>(override val data: D) : ViewState<D>()

    val isEmpty by lazy { this is Empty }
}
