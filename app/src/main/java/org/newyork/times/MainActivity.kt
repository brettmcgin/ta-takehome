package org.newyork.times

import android.annotation.SuppressLint
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.findNavController
import androidx.navigation.ui.setupWithNavController
import com.google.android.material.bottomnavigation.BottomNavigationView
import org.newyork.times.annotation.ApplicationScope
import toothpick.ktp.KTP
import toothpick.smoothie.lifecycle.closeOnDestroy

class MainActivity : AppCompatActivity() {
    private val navController by lazy { findNavController(R.id.nav_host_fragment) }

    @SuppressLint("DefaultLocale")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        KTP.openScopes(ApplicationScope::class.java, this)
            .closeOnDestroy(this)

        setContentView(R.layout.activity_main)

        bottomNavigationView()
    }

    private fun bottomNavigationView() =
        findViewById<BottomNavigationView>(R.id.nav_view)?.setupWithNavController(navController)
}
