package org.newyork.times.news

import android.content.Context
import android.graphics.Color
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.Drawable
import android.graphics.drawable.GradientDrawable
import androidx.core.content.ContextCompat.getDrawable
import androidx.core.graphics.ColorUtils
import androidx.palette.graphics.Palette
import androidx.recyclerview.widget.RecyclerView
import coil.load
import org.newyork.times.R.drawable.article_placeholder
import org.newyork.times.databinding.ArticleItemBinding

class ArticleViewHolder(
    private val item: ArticleItemBinding,
    private val onCLick: (Article) -> Unit,
    private val onLongCLick: (Article) -> Unit,
) : RecyclerView.ViewHolder(item.root) {
    @Suppress("MagicNumber")
    private val Int.isDark
        get() = ColorUtils.calculateLuminance(this) < 0.5

    fun bind(context: Context, article: Article) = with(item) {
        background.load(article.background) {
            crossfade(true)
            allowHardware(false)
            target(
                onError = {},
                onStart = { getDrawable(context, article_placeholder)?.setBackgroundAndGradient() },
                onSuccess = { it.setBackgroundAndGradient() }
            )
        }

        bindCard(article)

        title.text = article.headline
        description.text = article.abstract
    }

    private fun bindCard(article: Article) = with(item.card) {
        setOnLongClickListener {
            onLongCLick(article)
            true
        }

        setOnClickListener {
            onCLick(article)
        }
    }

    private fun Drawable.setBackgroundAndGradient() = with(item) {
        background.setImageDrawable(this@setBackgroundAndGradient)

        (this@setBackgroundAndGradient as? BitmapDrawable)?.bitmap?.let {
            Palette.from(it).generate()
        }?.dominantSwatch?.rgb?.let { rgb ->
            val textColor = if (rgb.isDark) Color.WHITE else Color.BLACK

            gradient.background = GradientDrawable(
                GradientDrawable.Orientation.BOTTOM_TOP,
                intArrayOf(rgb, Color.TRANSPARENT)
            )

            title.setTextColor(textColor)
            description.setTextColor(textColor)
        }
    }
}
