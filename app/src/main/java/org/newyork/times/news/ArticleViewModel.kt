package org.newyork.times.news

import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flatMapLatest
import kotlinx.coroutines.flow.map
import kotlinx.serialization.ExperimentalSerializationApi
import org.newyork.times.mvi.DispatcherProvider
import org.newyork.times.mvi.FlowStateViewModel
import org.newyork.times.mvi.ViewState
import org.newyork.times.news.ArticleViewModel.Intent
import toothpick.InjectConstructor

data class ArticleViewState(val articles: List<Article> = listOf())

@InjectConstructor
class ArticleViewModel(
    private val exhibitRepository: ArticleRepository,
    dispatcherProvider: DispatcherProvider
) : FlowStateViewModel<Intent, ArticleViewState>(dispatcherProvider.io) {

    fun load() = doAction(Intent.Load)

    sealed class Intent {
        object Load : Intent()
    }

    @ExperimentalSerializationApi
    @ExperimentalCoroutinesApi
    override fun transformFlow(intentStream: Flow<Intent>): Flow<ViewState<ArticleViewState>> =
        intentStream
            .flatMapLatest { intent ->
                when (intent) {
                    is Intent.Load -> exhibitRepository.exhibits.map { ArticleViewState(it) }
                }
            }.toViewState()
}
