package org.newyork.times.news

data class Article(
    val background: String,
    val headline: String,
    val url: String,
    val abstract: String,
)
