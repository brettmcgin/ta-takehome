package org.newyork.times.news

import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.serialization.ExperimentalSerializationApi
import org.newyork.times.service.NewYorkTimesClient
import toothpick.InjectConstructor

@InjectConstructor
data class ArticleRepository(
    private val client: NewYorkTimesClient,
) {
    @ExperimentalSerializationApi
    val exhibits: Flow<List<Article>> = flow { emit(client.fetchRecent()) }
}
