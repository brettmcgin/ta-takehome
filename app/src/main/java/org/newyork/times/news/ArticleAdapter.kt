package org.newyork.times.news

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import org.newyork.times.databinding.ArticleItemBinding.inflate

private class DiffCallback : DiffUtil.ItemCallback<Article>() {
    override fun areItemsTheSame(oldItem: Article, newItem: Article): Boolean =
        oldItem.url == newItem.url

    override fun areContentsTheSame(oldItem: Article, newItem: Article): Boolean =
        oldItem == newItem
}

class ArticleAdapter(
    private val context: Context,
    private val onClick: (Article) -> Unit = {},
    private val onLongClick: (Article) -> Unit = {},
) : ListAdapter<Article, ArticleViewHolder>(DiffCallback()) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        ArticleViewHolder(
            inflate(LayoutInflater.from(parent.context), parent, false),
            onCLick = { article -> onClick(article) },
            onLongCLick = { article -> onLongClick(article) }
        )

    override fun onBindViewHolder(holder: ArticleViewHolder, position: Int) =
        holder.bind(context, getItem(position))
}
