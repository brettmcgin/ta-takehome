package org.newyork.times.news

import android.content.Intent
import android.content.Intent.ACTION_SEND
import android.content.Intent.EXTRA_TEXT
import android.content.Intent.createChooser
import android.content.res.ColorStateList.valueOf
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.OnBackPressedCallback
import androidx.annotation.VisibleForTesting
import androidx.core.content.ContextCompat.getColor
import androidx.core.content.ContextCompat.getDrawable
import androidx.core.view.isVisible
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.GridLayoutManager
import org.newyork.times.R
import org.newyork.times.databinding.FragmentArticleListBinding
import org.newyork.times.extentions.viewBinding
import org.newyork.times.flow.collectIn
import org.newyork.times.injection.InjectionFragment
import org.newyork.times.mvi.ViewState.Data
import org.newyork.times.mvi.ViewState.Empty
import org.newyork.times.mvi.ViewState.Error
import toothpick.ktp.delegate.inject

inline fun onBackPressedCallback(
    enabled: Boolean = true,
    crossinline block: OnBackPressedCallback.() -> Unit
) = object : OnBackPressedCallback(enabled) {
    override fun handleOnBackPressed() = block(this)
}

class ArticleListFragment : InjectionFragment() {
    @VisibleForTesting
    val binding = viewBinding(FragmentArticleListBinding::inflate)

    private val exhibitsViewModel: ArticleViewModel by inject()
    private val articleAdapter by lazy {
        ArticleAdapter(
            requireContext(),
            onClick = { onArticleClick(it) },
            onLongClick = { onArticleLongClick(it) }
        )
    }

    private var articles: List<Article> = emptyList()
    private var columns = 1

    private val onBackPressedCallback = onBackPressedCallback(false) {
        binding.viewOrNull?.webView?.isVisible = false
        isEnabled = false
    }

    override fun onCreate(savedInstanceState: Bundle?) = super.onCreate(savedInstanceState).also {
        requireActivity().onBackPressedDispatcher.addCallback(this, onBackPressedCallback)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View = binding.inflate(inflater, container).root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) =
        super.onViewCreated(view, savedInstanceState).also { bind() }

    private fun bind() = with(binding.view) {
        bindArticles()
        bindSwipeRefresh()
        bindLayoutToggle()
        bindSearch()
    }

    private fun bindSearch() = with(binding.view.search) {
        setOnClickListener {
            text.toString().takeUnless { it.isEmpty() }?.let { query ->
                articleAdapter.submitList(articles.filter { article ->
                    article.headline.contains(query, true)
                })
            } ?: articleAdapter.submitList(articles)
        }
    }

    private fun bindArticles() = with(binding.view.articles) {
        adapter = articleAdapter
        layoutManager = GridLayoutManager(requireContext(), columns)
    }

    private fun bindSwipeRefresh() = with(binding.view.swipeRefresh) {
        setOnRefreshListener {
            isRefreshing = true
            exhibitsViewModel.load()
        }
    }

    private fun bindLayoutToggle() = with(binding.view.gridToggle) {
        backgroundTintList = valueOf(getColor(requireContext(), R.color.white))

        setOnClickListener {
            when (columns) {
                1 -> showList().also { columns = 2 }
                else -> showGrid().also { columns = 1 }
            }
        }
    }

    private fun showList() = with(binding.view) {
        gridToggle.background = getDrawable(requireContext(), R.drawable.ic_grid)
        articles.layoutManager = GridLayoutManager(requireContext(), columns)
    }

    private fun showGrid() = with(binding.view) {
        gridToggle.background = getDrawable(requireContext(), R.drawable.ic_list_bulleted)
        articles.layoutManager = GridLayoutManager(requireContext(), columns)
    }

    override fun onStart() {
        super.onStart()

        exhibitsViewModel.flow.collectIn(viewLifecycleOwner.lifecycleScope) { viewState ->
            when (viewState) {
                is Data -> onData(viewState.data.articles)
                is Empty -> exhibitsViewModel.load()
                is Error -> showEmpty()
            }

            binding.view.swipeRefresh.isRefreshing = false
        }
    }

    private fun onData(articles: List<Article>) {
        articleAdapter.submitList(articles)

        this.articles = articles

        if (articles.isEmpty()) showEmpty() else toggleContentVisibility(true)
    }

    private fun toggleContentVisibility(isVisible: Boolean = true) = with(binding.view) {
        articles.isVisible = isVisible
        errorMessage.isVisible = !isVisible
    }

    private fun showEmpty() = showError(getString(R.string.articles_empty))

    private fun showError(message: String) {
        toggleContentVisibility(false)
        binding.view.errorMessage.text = message
    }

    private fun onArticleClick(article: Article) = with(binding.view.webView) {
        isVisible = true
        loadUrl(article.url)
        onBackPressedCallback.isEnabled = true
    }

    private fun Article.shareIntent() = Intent().apply {
        action = ACTION_SEND
        putExtra(EXTRA_TEXT, url)
        type = "text/plain"
    }.run { createChooser(this, null) }

    private fun onArticleLongClick(article: Article) = startActivity(article.shareIntent())
}
