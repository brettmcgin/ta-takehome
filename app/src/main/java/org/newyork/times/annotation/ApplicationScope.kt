package org.newyork.times.annotation

import javax.inject.Scope

@Target(AnnotationTarget.CLASS)
@Retention(AnnotationRetention.RUNTIME)
@Scope
annotation class ApplicationScope
