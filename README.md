# TripActions Take Home

## Getting Started

Click a card to launch a detailed webview of that article
Long Click a card to share it's link

## Testing

Robolectric Fragment Scenario Test: `org.newyork.times.exhibit.ArticleListFragmentTest`
ViewModel Test: `org.newyork.times.exhibit.ArticleViewModelTest`

## Time Api Key

Add `API_KEY=YOUR_API_KEY` to local.properties file

## Example Images

![List View](screenshots/list.png)
![Grid View](screenshots/grid.png)
![Web View](screenshots/webview.png)
